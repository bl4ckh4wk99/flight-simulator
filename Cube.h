#pragma once
#include "gl\glew.h"
#include "shaders\Shader.h"
#include "glm\glm.hpp"


class Cube
{
private:
	float dim;
	unsigned int m_vaoID;		    // vertex array object
	unsigned int m_vboID[2];		// two VBOs - used for colours and vertex data
	GLuint ibo;                     //identifier for the triangle indices

	static const int NumberOfVertexCoords = 24;
	static const int NumberOfTriangleIndices = 36;
	static int numOfVerts;
	static int numOfTris;

	float verts[NumberOfVertexCoords];
	float tex[NumberOfVertexCoords];
	unsigned int tris[NumberOfTriangleIndices];
public:
	Cube();
	void setDim(float d);
	void constructGeometry(Shader* myShader, glm::vec2 centre, float width, float height, GLuint texture);
	void render();
};