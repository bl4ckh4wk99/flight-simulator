#pragma once
#include "Cube.h"
#include "Plane.h"
#include "nvImage.h"
#include <iostream>

class Hud{
private:
	Cube altDial;
	Cube speedDial;
	Cube speedHand;
	Cube altHand;
	Cube artPlane;
	Cube artVert;
	Cube crashed;
	Shader* hudShader;
	GLuint altDialTex, speedDialTex, handTex, artVertTex, artPlaneTex, crashedTex;
	Plane* plane;

	float speed, altitude, roll;
	bool planeCrashed;

public:
	Hud(){}
	Hud(Shader* hudShader, Plane* p);

	void draw();
	void update(double deltaT, float planesSpeed, float planesHeight, float planesRoll, bool planeCrashed);

	void resetHud(){this->speed = 0; this->altitude = 0; this->roll = 0;}
	GLuint LoadPNG(const char * filename);
};