#pragma once

#include "gl\glew.h"
#include "glm\glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm\gtc\matrix_inverse.hpp"
#include "Obj\OBJLoader.h"
#include "Octree\Octree.h"
#include "3DStruct\threeDModel.h"
#include "BoundingSphere.h"


class CollisionTests{
private:
	glm::mat4 ModelViewMatrix;

public:
	static float squared(float v) { return v * v; }

	static float isPointInTriangle(glm::vec3 point, glm::vec3 vert1, glm::vec3 vert2, glm::vec3 vert3);

	static bool isSphereCollidingWithBox(BoundingSphere bS, double minX, double minY, double minZ, double maxX, double maxY, double maxZ);

	static bool allOnSameSide(float* areas);

	static float SeperatingAxisTheorem(glm::vec3 tri1, glm::vec3 tri2, glm::vec3 tri3, glm::vec3 cube[8]);
};