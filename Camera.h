#pragma once

#include "gl\glew.h"
#include "glm\glm.hpp"
#include "Plane.h"

enum class Status{ First = 1, Third = 2, Static = 3};


class Camera{

private:
	glm::vec3 fPos, fLookat, fUp;
	glm::vec3 tPos, tLookat, tUp;
	glm::vec3 sPos, sLookat, sUp;
	float panAngle;
	Status active;
public:

	Camera();
	Camera(Plane* p, double deltaT);

	//update methods
	void update(Plane* p, double deltaT);
	void switchCam(Status newStat){active = newStat;}
	Status getStatus(){return active;}

	glm::vec3 getPos();
	glm::vec3 getLookat();
	glm::vec3 getUp();

	void panLeft();
	void panRight();
	void unPan();
	

};