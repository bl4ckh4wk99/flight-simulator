#include "Cube.h"
#include "shaders\Shader.h"

#include <iostream>
using namespace std;

int Cube::numOfTris = 12;
int Cube::numOfVerts = 8;

Cube::Cube()
{
	dim = 0.0;
}

void Cube::render()
{
	//draw objects
	glBindVertexArray(m_vaoID);		// select VAO
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glDrawElements(GL_TRIANGLES, numOfTris*3, GL_UNSIGNED_INT, 0);
			
	// Done
	
	glBindVertexArray(0); //unbind the vertex array object
	
}

void Cube::constructGeometry(Shader* myShader, glm::vec2 centre, float width, float height, GLuint texture)
{
	// First simple object
	verts[0] = centre.x-width;   verts[ 1] = centre.y-height;  verts[ 2] = -1;
	verts[3] = centre.x-width;   verts[ 4] = centre.y+height;  verts[ 5] = -1;
	verts[6] = centre.x+width;   verts[ 7] = centre.y+height;  verts[ 8] = -1;
	verts[9] = centre.x+width;   verts[10] = centre.y-height;  verts[11] = -1;

	verts[12] = centre.x-width;   verts[13] = centre.y-height;  verts[14] = 1;
	verts[15] = centre.x-width;   verts[16] = centre.y+height;  verts[17] = 1;
	verts[18] = centre.x+width;   verts[19] = centre.y+height;  verts[20] = 1;
	verts[21] = centre.x+width;   verts[22] = centre.y-height;  verts[23] = 1;

	tex[0] = 0.0;   tex[ 1] = 0.0;  tex[ 2] = 0.0;
	tex[3] = 0.0;   tex[ 4] = 1.0;  tex[ 5] = 0.0;
	tex[6] = 1.0;   tex[ 7] = 1.0;  tex[ 8] = 0.0;
	tex[9] = 1.0;   tex[10] = 0.0;  tex[11] = 0.0;

	tex[12] = 0.0;   tex[13] = 0.0;  tex[14] = 0.0;
	tex[15] = 0.0;   tex[16] = 1.0;  tex[17] = 0.0;
	tex[18] = 1.0;   tex[19] = 1.0;  tex[20] = 0.0;
	tex[21] = 1.0;   tex[22] = 0.0;  tex[23] = 0.0;
	
	tris[0]=0; tris[1]=1; tris[2]=2;
	tris[3]=0; tris[4]=2; tris[5]=3;
	tris[6]=4; tris[7]=6; tris[8]=5;
	tris[9]=4; tris[10]=7; tris[11]=6;
	tris[12]=1; tris[13]=5; tris[14]=6;
	tris[15]=1; tris[16]=6; tris[17]=2;
	tris[18]=0; tris[19]=7; tris[20]=4;
	tris[21]=0; tris[22]=3; tris[23]=7;
	tris[24]=0; tris[25]=5; tris[26]=1;
	tris[27]=0; tris[28]=4; tris[29]=5;
	tris[30]=3; tris[31]=2; tris[32]=7;
	tris[33]=2; tris[34]=6; tris[35]=7;
		
	// VAO allocation
	glGenVertexArrays(1, &m_vaoID);

	// First VAO setup
	glBindVertexArray(m_vaoID);
	
	glGenBuffers(2, m_vboID);
	
	glBindBuffer(GL_ARRAY_BUFFER, m_vboID[0]);
	//initialises data storage of vertex buffer object
	glBufferData(GL_ARRAY_BUFFER, numOfVerts*3*sizeof(GLfloat), verts, GL_STATIC_DRAW);
	GLint vertexLocation= glGetAttribLocation(myShader->handle(), "in_Position");
	glVertexAttribPointer(vertexLocation, 3, GL_FLOAT, GL_FALSE, 0, 0); 
	glEnableVertexAttribArray(vertexLocation);

	
	glBindBuffer(GL_ARRAY_BUFFER, m_vboID[1]);
	glBufferData(GL_ARRAY_BUFFER, numOfVerts*3*sizeof(GLfloat), tex, GL_STATIC_DRAW);
	GLint texCoordLocation= glGetAttribLocation(myShader->handle(), "in_TexCoord");
	glVertexAttribPointer(texCoordLocation, 3, GL_FLOAT, GL_FALSE, 0, 0); 
	glEnableVertexAttribArray(texCoordLocation);


	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	glGenBuffers(1, &ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numOfTris * 3 * sizeof(unsigned int), tris, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glEnableVertexAttribArray(0);

	glBindVertexArray(0);
}


void Cube::setDim(float d)
{
	dim = d;
}