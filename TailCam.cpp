#include "TailCam.h"
#include <iostream>
using namespace std;

TailCam::TailCam(float x, float y, float z){
	this->pos = glm::vec3(x, y,z);
}

void TailCam::updatePos(Plane* p){
	glm::vec3 planePos = p->getPosition();

	this->pos = planePos + (p->getDirection()* 50.0f);

	//cout << "Plane Pos " <<p->getPosition().x << ", " << p->getPosition().y << " , " << p->getPosition().z << endl;
	//cout << "Plane Direction "<< p->getDirection().x << ", " << p->getDirection().y << " , " << p->getDirection().z << endl;
	//cout << "Cam Pos "<< this->pos.x << ", " << this->pos.y << " , " << this->pos.z << endl;
}