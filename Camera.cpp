#include "Camera.h"

Camera::Camera(){
	//always set active cam to third person and pan angle to 0
	active = Status::Third;
	panAngle = 0.0f;
}

Camera::Camera(Plane* p, double deltaT){
	//always set active cam to third person and pan angle to 0
	active = Status::Third;
	panAngle = 0.0f;
	update(p, deltaT);
}

void Camera::update(Plane* p, double deltaT){
	switch(active){
		case Status::First:
			{
				//get plane pos
				glm::vec3 planePos = p->getPosition();
				//get the lookout of the camera, and minus the planes pos to rotate round origin
				glm::vec3 tempLookAt = p->getPosition() + (p->getDirection() * 10.0f) + (p->getVertical() * 5.0f);
				tempLookAt = tempLookAt - planePos;
				//rotate around origin
				glm::mat4 rotMat =  glm::rotate(glm::mat4(1.0), panAngle, p->getVertical());
				glm::vec3 newLookAt = glm::vec3(rotMat * glm::vec4(tempLookAt,1.0));
				//move lookat back to plane
				newLookAt = newLookAt + planePos;

				fPos = (p->getPosition() + p->getVertical() * 3.0f) - (p->getDirection() * 3.0f);
				fLookat = newLookAt;
				fUp  =  p->getVertical();
			}
			break;
		case Status::Third:
			{
				//get plane pos
				glm::vec3 planePos = p->getPosition();
				//get the lookout of the camera, and minus the planes pos to rotate round origin

				glm::vec3 tempLookAt;
				glm::vec3 tempPos;
				if(panAngle == 0){
				//**SOFT ATTACH**
					glm::vec3 distToLookAt =  (p->getPosition() + (p->getDirection() * 10.0f)) - this->tLookat;
					tempLookAt =  this->tLookat + (float(5.0f*deltaT) * distToLookAt);
					glm::vec3 distToPlane = ((p->getPosition() - (p->getDirection() * 40.0f) + (p->getVertical() * 3.0f)) - this->tPos);
					tempPos =  this->tPos + (float(5.0f*deltaT) * distToPlane);
					tPos = tempPos;
					tLookat = tempLookAt;

				}else{
				//**NORMAL CAMERA
					tempLookAt = p->getPosition() + (p->getDirection() * 10.0f);
					tempLookAt = tempLookAt - planePos;
					tempPos = (p->getPosition() - (p->getDirection() * 40.0f) + (p->getVertical() * 3.0f));
					tempPos = tempPos - planePos;
					//rotate around origin
					glm::mat4 rotMat =  glm::rotate(glm::mat4(1.0), panAngle, p->getVertical());
					glm::vec3 newLookAt = glm::vec3(rotMat * glm::vec4(tempLookAt,1.0));
					glm::vec3 newPos = glm::vec3(rotMat * glm::vec4(tempPos,1.0));
					//move lookat back to plane
					newLookAt = newLookAt + planePos;
					newPos = newPos + planePos;
					tPos = newPos;
					tLookat = newLookAt;
				}
				tUp = p->getVertical();
			}
			break;
		case Status::Static:
			{
				//static cam update is simple, just update the lookat, keep others constant
				sPos = glm::vec3(500,500,500);
				sLookat = p->getPosition();
				sUp = glm::vec3(0,1,0);
			}
			break;
	}
}

glm::vec3 Camera::getPos(){
	switch(active){
		case Status::First:
			return fPos;
			break;
		case Status::Third:
			return tPos;
			break;
		case Status::Static:
			return sPos;
			break;
	}
}
glm::vec3 Camera::getLookat(){
	switch(active){
		case Status::First:
			return fLookat;
			break;
		case Status::Third:
			return tLookat;
			break;
		case Status::Static:
			return sLookat;
			break;
	}
}
glm::vec3 Camera::getUp(){
	switch(active){
		case Status::First:
			return fUp;
			break;
		case Status::Third:
			return tUp;
			break;
		case Status::Static:
			return sUp;
			break;
	}
}

void Camera::panLeft(){
	switch(active){
		case Status::First:
			panAngle += 2.0f;
			if(panAngle > 75.0f)
				panAngle = 75.0f;
			break;
		case Status::Third:
			panAngle -= 0.5f;
			if(panAngle < -175.0f)
				panAngle = -175.0f;
			break;
	}
}
void Camera::panRight(){
	switch(active){
		case Status::First:
			panAngle -= 2.0f;
			if(panAngle < -75.0f)
				panAngle = -75.0f;
			break;
		case Status::Third:
			panAngle += 0.5f;
			if(panAngle > 175.0f)
				panAngle = 175.0f;
			break;
	}
}
void Camera::unPan(){
	if(panAngle < 6.0f && panAngle > -6.0f){
		panAngle = 0.0f;
	}
	else{
		if(panAngle > 0.0f)
			panAngle -= 6.0f;
		else
			panAngle += 6.0f;
	}
}





