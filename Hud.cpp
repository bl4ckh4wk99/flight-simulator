#include "Hud.h"

Hud::Hud(Shader* hudShader, Plane* p){
	this->hudShader = hudShader;
	this->plane = p;
	glm::vec2 altDialCen = glm::vec2(0,0);
	glm::vec2 speedDialCen = glm::vec2(0,0);
	altDial.constructGeometry(hudShader,altDialCen,0.25,0.25, altDialTex);
	speedDial.constructGeometry(hudShader,speedDialCen,0.25,0.25, speedDialTex);

	glm::vec2 altHandCen = glm::vec2(0,0);
	glm::vec2 speedHandCen = glm::vec2(0,0);
	speedHand.constructGeometry(hudShader,altHandCen,0.025,0.15,handTex);
	altHand.constructGeometry(hudShader,speedHandCen,0.025,0.15,handTex);

	glm::vec2 artVertCen = glm::vec2(0,0);
	glm::vec2 artPlaneCen = glm::vec2(0,0);
	artVert.constructGeometry(hudShader,artVertCen,0.1,0.25,artVertTex);
	artPlane.constructGeometry(hudShader,artPlaneCen,0.20,0.4,artPlaneTex);

	glm::vec2 crashedCen = glm::vec2(0,0);
	crashed.constructGeometry(hudShader,crashedCen,1.0,0.25,crashedTex);

	this->roll = 0;

	altDialTex = LoadPNG("TestModels/Textures/altDial.png");
	speedDialTex = LoadPNG("TestModels/Textures/speedDial.png");
	handTex = LoadPNG("TestModels/Textures/dialHand.png");
	artPlaneTex = LoadPNG("TestModels/Textures/artPlane.png");
	artVertTex = LoadPNG("TestModels/Textures/artVert.png");
	crashedTex = LoadPNG("TestModels/Textures/crashed.png");
}

void Hud::draw(){
	glDisable(GL_DEPTH_TEST);
	if(!planeCrashed){
		//altitude dial

		glm::mat4 ModelViewMatrix = glm::translate(glm::mat4(1.0),glm::vec3(-0.75,-0.75,0));
		glUniformMatrix4fv(glGetUniformLocation(hudShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
	
		glUniform1i(glGetUniformLocation(hudShader->handle(), "DiffuseMap"), 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, altDialTex);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		altDial.render();
	
		ModelViewMatrix = glm::rotate(ModelViewMatrix, this->altitude, glm::vec3(0,0,1));
		glUniformMatrix4fv(glGetUniformLocation(hudShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
		glUniform1i(glGetUniformLocation(hudShader->handle(), "DiffuseMap"), 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, handTex);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		altHand.render();

		//speed dial
		ModelViewMatrix = glm::translate(glm::mat4(1.0),glm::vec3(0.75,-0.75,0));
		glUniformMatrix4fv(glGetUniformLocation(hudShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
	
		glUniform1i(glGetUniformLocation(hudShader->handle(), "DiffuseMap"), 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, speedDialTex);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		speedDial.render();
	
		ModelViewMatrix = glm::rotate(ModelViewMatrix, this->speed, glm::vec3(0,0,1));
		glUniformMatrix4fv(glGetUniformLocation(hudShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
	
		glUniform1i(glGetUniformLocation(hudShader->handle(), "DiffuseMap"), 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, handTex);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		speedHand.render();
	
		//artificial horizon dial
		ModelViewMatrix = glm::translate(glm::mat4(1.0),glm::vec3(0.0,-0.75,0));
		glUniformMatrix4fv(glGetUniformLocation(hudShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
	
		glUniform1i(glGetUniformLocation(hudShader->handle(), "DiffuseMap"), 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, artVertTex);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		artVert.render();


		glm::quat tempQuat = glm::quat(glm::vec3(0,0,roll));
		glm::mat4 rotate = glm::toMat4(tempQuat);
		ModelViewMatrix = ModelViewMatrix * (2*rotate);
		//ModelViewMatrix = glm::rotate(ModelViewMatrix, this->roll, glm::vec3(0,0,1));
		glUniformMatrix4fv(glGetUniformLocation(hudShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);

		glUniform1i(glGetUniformLocation(hudShader->handle(), "DiffuseMap"), 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, artPlaneTex);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		artPlane.render();
	}
	if(planeCrashed){
		glm::mat4 ModelViewMatrix = glm::translate(glm::mat4(1.0),glm::vec3(0.0,-0.75,0));
		glUniformMatrix4fv(glGetUniformLocation(hudShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);

		glUniform1i(glGetUniformLocation(hudShader->handle(), "DiffuseMap"), 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, crashedTex);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		crashed.render();

	}
	

	glEnable(GL_DEPTH_TEST);
}

void Hud::update(double deltaT, float planesSpeed, float planesHeight, float planesRoll, bool planeCrashed){
	this->speed = -sqrt(pow(planesSpeed*2,2));
	this->altitude = -(planesHeight/2);
	this->roll = (this->roll - planesRoll);
	this->planeCrashed = planeCrashed;
}


GLuint Hud::LoadPNG(const char * filename){
	GLuint retVal;
		//following to be placed in the init function
		nv::Image img;
		// Return true on success
		if (img.loadImageFromFile(filename))
		{
			glGenTextures(1, &retVal);
			glBindTexture(GL_TEXTURE_2D, retVal);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
			glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
			glTexImage2D(GL_TEXTURE_2D, 0, img.getInternalFormat(), img.getWidth(),img.getHeight(), 0, img.getFormat(), img.getType(), img.getLevel(0));
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
		}
		else
			MessageBox(NULL, "Failed to load texture", "End of the world", MB_OK | MB_ICONINFORMATION);

		std::cout << "Loaded Texture" << std::endl;
		return retVal;
}