#pragma once

#include "gl\glew.h"
#include "glm\glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm\gtc\matrix_inverse.hpp"
#include "shaders\Shader.h"
#include "Obj\OBJLoader.h"
#include "Octree\Octree.h"
#include "3DStruct\threeDModel.h"
#include "glm\gtx\quaternion.hpp"
#include "BoundingSphere.h"
#include "CollisionTests.h"

class Shader;

#define GRAVITY 10.0f

class Plane {
private:
  glm::vec3 currentRot;
  glm::vec3 position;
  double speed;
  Shader* basicShader;
  glm::quat currentQuat;
  bool stalled, landed, crashed;
  vector <ThreeDModel*> planeModel;
  BoundingSphere bSphere;
  double deltaT;
  bool leftWingBroke, rightWingBroke, leftWheelBroke, rightWheelBroke;
public:
  //Constructors
  Plane(Shader* myShader, vector <ThreeDModel*> planeModel);
  Plane();

  //Draw and updates
  void draw();
  void update(double deltaT);
  void fly();
  void collisionResponse(glm::vec3 tri1, glm::vec3 tri2, glm::vec3 tri3, float response);
  void teleport(glm::vec3 newPosition);

  //Getters Directions
  glm::vec3 getPosition();
  glm::vec3 getDirection();
  glm::vec3 getVertical();
  glm::vec3 getRight();
  
  //Plane Movement
  double getSpeed(); 
  void setSpeed(double newSpeed);
  float pitch(bool positive, float currentRot);
  float roll(bool positive, float currentRot);
  float yaw(bool positive, float currentRot);
  glm::vec3 specialCases(glm::vec3 rot);

  //Quaternians
  glm::quat getCurrentQuat(){return currentQuat;}
  void setCurrentQuat(glm::quat temp){this->currentQuat = temp;}
  void resetRot(){this->currentRot = glm::vec3(0,0,0);}
  glm::vec3 getRot(){return this->currentRot;}
  void setRot(glm::vec3 newRot){this->currentRot = newRot;}

  //Plane Status Getter & Setters
  bool getStalledStatus(){return this->stalled;}
  void setStalled(bool stalled){this->stalled = stalled;}
  bool getLandedStatus(){return this->landed;}
  void setLanded(bool landed){this->landed = landed;}
  void setCrashed(bool crashed){this->crashed = crashed;}
  bool getCrashed(){return this->crashed;}
	//wings
  void newLeftWing(){this->leftWingBroke = false;}
  bool getLeftWingStatus(){return this->leftWingBroke;}
  void newRightWing(){this->rightWingBroke = false;}
  bool getRightWingStatus(){return this->rightWingBroke;}
  	//Wheels
  void newLeftWheel(){this->leftWheelBroke = false;}
  bool getLeftWheeltatus(){return this->leftWheelBroke;}
  void newRightWheel(){this->rightWheelBroke = false;}
  bool getRightWheelStatus(){return this->rightWheelBroke;}

  //Collision Methods
  void detectCollisionRoot(vector <ThreeDModel*> models);
  void detectCollisionNode(Octree* oct, ThreeDModel* model);
  void detectCollisionTri(Octree* leaf, ThreeDModel* model);
  glm::vec3* getBBVertices(ThreeDModel* model);
};
