#include "CollisionTests.h"

//check if a point is within a triangle
float CollisionTests::isPointInTriangle(glm::vec3 point, glm::vec3 vert1, glm::vec3 vert2, glm::vec3 vert3){

	glm::mat4 tet = glm::mat4(glm::vec4(vert1,1.0),glm::vec4(vert2,1.0),glm::vec4(vert3,1.0),glm::vec4(point,1.0));
	
	float vol = glm::determinant(tet);

	return vol;
}

//check if a sphere is colliding with a box
bool CollisionTests::isSphereCollidingWithBox(BoundingSphere bS, double minX, double minY, double minZ, double maxX, double maxY, double maxZ){

	float dist_squared = squared(bS.getRadius());

	if (bS.getCentre().x < minX) dist_squared -= squared(bS.getCentre().x - minX);
    else if (bS.getCentre().x > maxX) dist_squared -= squared(bS.getCentre().x - maxX);
    if (bS.getCentre().y < minY) dist_squared -= squared(bS.getCentre().y - minY);
    else if (bS.getCentre().y > maxY) dist_squared -= squared(bS.getCentre().y - maxY);
    if (bS.getCentre().z < minZ) dist_squared -= squared(bS.getCentre().z - minZ);
    else if (bS.getCentre().z > maxZ) dist_squared -= squared(bS.getCentre().z - maxZ);
    return dist_squared > 0;
}
//check if all points are on the same side of a triangle ( on left test)
bool CollisionTests::allOnSameSide(float* areas){
	bool positive, temp;
	positive = areas[0] > 0;
	for(int i = 1; i < 8; i++){
		if(areas[i] > 0)
			temp = true;
		else
			temp = false;
		if(positive != temp)
			return false;
	}
	return true;
}
//check if a triangle and a box are colliding using SATs
float CollisionTests::SeperatingAxisTheorem(glm::vec3 tri1, glm::vec3 tri2, glm::vec3 tri3, glm::vec3 cube[8]){
	//create variables
	vector<glm::vec3> projections;
	float minOverlap = INT_MAX;
	//cube normals
	glm::vec3 cube1 = glm::normalize(glm::cross(cube[3]- cube[1] ,cube[5] - cube[1]));
	projections.push_back(cube1);
	glm::vec3 cube2 = glm::normalize(glm::cross(cube[3]- cube[2] ,cube[7] - cube[2]));
	projections.push_back(cube2);
	glm::vec3 cube3 = glm::normalize(glm::cross(cube[5]- cube[4] ,cube[6] - cube[4]));
	projections.push_back(cube3);

	//tri normal
	projections.push_back(glm::normalize(glm::cross(tri2- tri1 ,tri3 - tri1)));

	//cube normals * tri edges
	projections.push_back((glm::cross(cube1 , (tri1 - tri2))));
	projections.push_back((glm::cross(cube1 , (tri2 - tri3))));
	projections.push_back((glm::cross(cube1 , (tri3 - tri1))));
	projections.push_back((glm::cross(cube2 , (tri1 - tri2))));
	projections.push_back((glm::cross(cube2 , (tri2 - tri3))));
	projections.push_back((glm::cross(cube2 , (tri3 - tri1))));
	projections.push_back((glm::cross(cube3 , (tri1 - tri2))));
	projections.push_back((glm::cross(cube3 , (tri2 - tri3))));
	projections.push_back((glm::cross(cube3 , (tri3 - tri1))));
	//loop over the projections and use each one to project the values of the cube and triangle onto and check for overlap
	for each(glm::vec3 proj in projections){
		float minT = INT_MAX;
		float maxT = -INT_MAX;
		float minC = INT_MAX;
		float maxC = -INT_MAX;
		//find min and max values for cube
		for(int i = 0; i < 8; i++){
			float temp = glm::dot(proj,cube[i]);
			if(temp < minC)
				minC = temp;
			if(temp > maxC)
				maxC = temp;
		}
		//find min and max values for triangle
		float temp = glm::dot(proj,tri1);
		if(temp < minT)
			minT = temp;
		if(temp > maxT)
			maxT = temp;
		temp = glm::dot(proj,tri2);
		if(temp < minT)
			minT = temp;
		if(temp > maxT)
			maxT = temp;
		temp = glm::dot(proj,tri3);
		if(temp < minT)
			minT = temp;
		if(temp > maxT)
			maxT = temp;

		//check for overlap, if none return no collision
		if(maxC < minT || minC > maxT){
			return 0;
		}
		
		float calc = maxC - minT;
		float calc2 = minC - maxT;
		if(calc < minOverlap)
			minOverlap = calc;
		if(calc2 < minOverlap)
			minOverlap = calc2;
	}
	//if all vertices are projected and overlap occurs on each then they have collided
	return minOverlap;

}