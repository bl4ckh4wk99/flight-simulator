#pragma once
#include "gl\glew.h"
#include "glm\glm.hpp"
#include "3dStruct\Sphere.h"

class BoundingSphere{
private:
	glm::vec3 centre;
	float radius;
	Sphere sphere;
public:
	BoundingSphere(){}
	BoundingSphere(glm::vec3 centre, float rad, Shader* myShader);
	void draw();

	void updateCentre(glm::vec3 newPos);
	glm::vec3 getCentre(){return this->centre;}
	float getRadius(){return this->radius;}
};