#pragma once

#include "Camera.h"

class TailCam : public Camera{
private:

public:
	TailCam(float x, float y, float z);
	void updatePos(Plane* p);
};
