#include "BoundingSphere.h"

BoundingSphere::BoundingSphere(glm::vec3 centre, float rad, Shader* myShader){
	this->centre = centre;
	this->radius = rad;
	sphere.setCentre(centre.x,centre.y,centre.z);
	sphere.setRadius(rad);
	sphere.constructGeometry(myShader, 15);
}

void BoundingSphere::updateCentre(glm::vec3 newPos){
	this->centre = newPos;
	sphere.setCentre(centre.x,centre.y,centre.z);
}

void BoundingSphere::draw(){
	sphere.render();
}