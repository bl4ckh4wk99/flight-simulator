#include "Plane.h"

#include <iostream>
using namespace std;

Plane::Plane(){
	position = glm::vec3(0, 20, 0);
	speed    = 0.01;
	currentQuat = glm::quat(glm::vec3(0.0,0.0,0.0));
	currentRot = glm::vec3(0,0,0);
	stalled = false;
	landed = true; //should be true eventually
	crashed = false;
	leftWheelBroke = false;
	leftWingBroke = false;
	rightWheelBroke = false;
	rightWingBroke = false;
}

 Plane::Plane(Shader* basicShader, vector <ThreeDModel*> planeModel){
	
	position = glm::vec3(0, 20, 0);
	speed    = 0.01;
	currentQuat = glm::quat(glm::vec3(0.0,0.0,0.0));
	currentRot = glm::vec3(0,0,0);
	this->basicShader = basicShader;
	stalled = false;
	landed = true; //should be true eventually
	crashed = false;
	leftWheelBroke = false;
	leftWingBroke = false;
	rightWheelBroke = false;
	rightWingBroke = false;
	this->planeModel = planeModel;
	bSphere = BoundingSphere(position, 25.0f, basicShader);
 }

 void Plane::draw(){
	 bSphere.draw();
 }

 void Plane::update(double deltaT){
	 //get deltaT
	 this->deltaT = deltaT;
	 //move the plane base on its direction and speed
	 this->fly();
	 //apply gravity
	 this->position.y -= (deltaT * GRAVITY);
	 //update bounding spheres centre
	 bSphere.updateCentre(this->position);
 }


glm::vec3 Plane::getPosition() {
	return position;
}



void Plane::collisionResponse(glm::vec3 tri1, glm::vec3 tri2 , glm::vec3 tri3, float response){
	//respond to collision byt moving plane in the direction of the normal of the triangle collided with
	glm::vec3 dir = -glm::normalize(glm::cross(tri2-tri1, tri3-tri1));
	//by the correct amount in each direction
	float speedX = glm::abs(this->getDirection().x * speed);
	float speedY = glm::abs(this->getDirection().y * speed);
	float speedZ = glm::abs(this->getDirection().z * speed);
	position -= glm::vec3((speedX * deltaT) * dir.x, ((speedY + GRAVITY)*deltaT)* dir.y, (speedZ * deltaT) * dir.z);
}

void Plane::teleport(glm::vec3 newPosition) {
	  position = newPosition;
}

//get the direction vector from the orientation quaternion
glm::vec3 Plane::getDirection() {
	glm::mat4 orient = glm::toMat4(currentQuat);
	return glm::vec3(orient[2][0],orient[2][1],orient[2][2]);
}
//get the vertical vector from the orientation quaternion
glm::vec3 Plane::getVertical() {
	glm::mat4 orient = glm::toMat4(currentQuat);
	return glm::vec3(orient[1][0],orient[1][1],orient[1][2]);
}
//get the right vector from the orientation quaternion
glm::vec3 Plane::getRight(){
	glm::mat4 orient = glm::toMat4(currentQuat);
	return glm::vec3(orient[0][0],orient[0][2],orient[0][3]);
}
  
/****************************************************************************************************************************************
														MOVEMENT
*****************************************************************************************************************************************/
double Plane::getSpeed() {
	return speed;
}
  
void Plane::setSpeed(double newSpeed) {
	speed = newSpeed;
}
//move the plane in the foward direction vector by the speed in relation to deltaT
void Plane::fly() {
	glm::vec3 forward = getDirection();
	position += glm::vec3((speed*deltaT) * forward.x, (speed*deltaT) * forward.y, (speed*deltaT) * forward.z);
}
//pitch the plane by a constant amount in relation to delta speed
float Plane::pitch(bool positive, float currentRot){
	float deltaSpeed = this->getSpeed() * deltaT;
	if(positive){
		float rot = currentRot += (0.002f * deltaSpeed + 0.001);
		return rot;
	}else{
		float rot = currentRot -= (0.002f * deltaSpeed + 0.001);
		return rot;
	}
	
}
//roll the plane by a constant amount in relation to delta speed
float Plane::roll(bool positive, float currentRot){
	float deltaSpeed = this->getSpeed() * deltaT;
	if(positive){
		float rot = currentRot += (0.005f * deltaSpeed + 0.001);
		return rot;
	}else{
		float rot = currentRot -= (0.005f * deltaSpeed + 0.001);
		return rot;
	}
}
//yaw the plane by a constant amount in relation to delta speed
float Plane::yaw(bool positive, float currentRot){
	float deltaSpeed = this->getSpeed() * deltaT;
	if(positive){
		float rot = currentRot += (0.001f * deltaSpeed + 0.001);
		return rot;
	}else{
		float rot = currentRot -= (0.001f * deltaSpeed + 0.001);
		return rot;
	}
}
//For Andy
glm::vec3 Plane::specialCases(glm::vec3 rot){
	//if a wing is missing then pitch is changed until the plane is pointing downwards
	if(this->getLeftWingStatus() || this->getRightWingStatus()){
		rot.x = 0;
		glm::mat4 orientation = glm::toMat4(currentQuat);

		float distToDie = 0.0f;
		if(orientation[2][1] > 0.0)
			distToDie = orientation[2][1] - -0.7;
		else
			distToDie =  orientation[2][1] - -0.7;

		orientation[2][1] = orientation[2][1] - (distToDie * deltaT);
		this->currentQuat = glm::toQuat(orientation);
	}
	//if plane is outside of playable area, start pitching towards ground
	float planesPosx = pow(this->position.x,2);
	float planesPosy = pow(this->position.y,2);
	float planesPosz = pow(this->position.z,2);
	float planePos = planesPosx + planesPosy + planesPosz;
	float maxDist = pow(1500,2);
	if(planePos > maxDist){
		rot.x = 0;
		glm::mat4 orientation = glm::toMat4(currentQuat);

		float distToDie = 0.0f;
		if(orientation[2][1] > 0.0)
			distToDie = orientation[2][1] - -0.7;
		else
			distToDie =  orientation[2][1] - -0.7;

		orientation[2][1] = orientation[2][1] - (distToDie * deltaT);
		this->currentQuat = glm::toQuat(orientation);
	}


	//if a single wing is missing apply constant rotation in that direction
	float deltaSpeed = this->getSpeed() * deltaT;
	if(this->getLeftWingStatus()){
		rot.z -= 3*this->roll(1,rot.z);
	}
	if(this->getRightWingStatus()){
		rot.z -= 3*this->roll(0,rot.z);
	}
	//Deal with the planes special movement cases
	if(this->getLandedStatus()){
		//Plane is on teh floor
		if(this->getPosition().y > 15)
			this->setLanded(false);
		switch(this->getStalledStatus()){
			//Plane cant stall when landed so set stall to false;
			case true:
				this->setStalled(false);
				break;
				//Landed and not stalled, plane cant roll whilst landed and cant pitch until certain speed
			case false:
				if(this->getSpeed() < 55)
					rot.x = 0;
				rot.z =0;
				break;
		}
	}else{
		//Plane is not on teh floor
		//cannot reverse in the air silly
		if(this->speed < 0)
			this->speed = 0;
		if(this->getPosition().y < 15)
			this->setLanded(true);
		switch(this->getStalledStatus()){
			//Plane is stalled in the air so decrease speed until 0
			case true:
				this->setSpeed(this->getSpeed() - 0.1);
				if(this->getSpeed() < 0)
					this->setSpeed(0);
				this->position.y - 10.f;
				this->setStalled(false);
				break;
				//Ideal flying, not stalled and not landed so check for status changes
			case false:
				//too vertical so stall
				if(this->getDirection().y > 0.85)
					this->setStalled(true);
				break;
				//too high so stall
				if(this->getPosition().y > 500)
					this->setStalled(true);
				//too far from centre
				float distFromCentre = CollisionTests::squared(this->getPosition().x) + CollisionTests::squared(this->getPosition().z);
				if(distFromCentre > 2250000) //squared radius of 1500 = 2250000
					this->setStalled(true);
		}
	}
	return rot;	
}

/****************************************************************************************************************************************
														COLLISION
*****************************************************************************************************************************************/

//get the verticies of the bounding box of the planes components model
glm::vec3* Plane::getBBVertices(ThreeDModel* model){
	glm::vec3* verticies = new glm::vec3[8];

	glm::mat4 trans = glm::translate(glm::mat4(1.0),this->position);
	glm::mat4 rot = glm::toMat4(currentQuat);

	glm::mat4 modelMatrix = trans * rot;

	//Top Face; 1,3,5,7
	verticies[1] = glm::vec3(modelMatrix * glm::vec4(glm::vec3(model->octree->getMinX(),model->octree->getMaxY(),model->octree->getMinZ()),1.0));
	verticies[3] = glm::vec3(modelMatrix * glm::vec4(glm::vec3(model->octree->getMinX(),model->octree->getMaxY(),model->octree->getMaxZ()),1.0));
	verticies[5] = glm::vec3(modelMatrix * glm::vec4(glm::vec3(model->octree->getMaxX(),model->octree->getMaxY(),model->octree->getMinZ()),1.0));
	verticies[7] = glm::vec3(modelMatrix * glm::vec4(glm::vec3(model->octree->getMaxX(),model->octree->getMaxY(),model->octree->getMaxZ()),1.0));

	//Front Face; 3,7,2,6
	verticies[2] = glm::vec3(modelMatrix * glm::vec4(glm::vec3(model->octree->getMinX(),model->octree->getMinY(),model->octree->getMaxZ()),1.0));
	verticies[6] = glm::vec3(modelMatrix * glm::vec4(glm::vec3(model->octree->getMaxX(),model->octree->getMinY(),model->octree->getMaxZ()),1.0));

	//Right Face; 5,7,6,4
	verticies[4] = glm::vec3(modelMatrix * glm::vec4(glm::vec3(model->octree->getMaxX(),model->octree->getMinY(),model->octree->getMinZ()),1.0));

	verticies[0] = glm::vec3(modelMatrix * glm::vec4(glm::vec3(model->octree->getMinX(),model->octree->getMinY(),model->octree->getMinZ()),1.0));

	return verticies;
}

//check for collisions in the root node of the octree
void Plane::detectCollisionRoot(vector<ThreeDModel*> models){
	for each(ThreeDModel* m in models){
		Octree* oct = m->octree;
		bool collision = CollisionTests::isSphereCollidingWithBox(bSphere,oct->getMinX(),oct->getMinY(),oct->getMinZ(),
																	oct->getMaxX(),oct->getMaxY(),oct->getMaxZ());
		if(collision){
			detectCollisionNode(oct, m);
		}else{
			//no collision
		}
	}
}
//check for collision in the node of the octree (recusively called)
void Plane::detectCollisionNode(Octree* node, ThreeDModel* model){
	for(int i=0; i < 8; i++){
		if(node->getChild(i) != NULL){
			Octree* octChild = node->getChild(i);
			bool collision = CollisionTests::isSphereCollidingWithBox(bSphere,octChild->getMinX(),octChild->getMinY(),octChild->getMinZ(),
																		octChild->getMaxX(),octChild->getMaxY(),octChild->getMaxZ());
			if(collision){
				if(octChild->getLevel() == MAX_DEPTH){
					detectCollisionTri(octChild, model);
				}
				else{
					detectCollisionNode(octChild, model);
				}
			}
			else{
				//no collision
			}
		}
	}

}
//detect collision against the triangles in the octree child and the planes components
void Plane::detectCollisionTri(Octree* leaf,ThreeDModel* model){
	//cout << "in detect tris" << endl;
	int numOfTri = leaf->getPrimitiveListSize();
	if(numOfTri > 0){
		int* tris = leaf->getPrimitiveList();
		bool wheelCollision = false;
		for(int i =0; i < numOfTri; i++){

			//get the verticies of the planes models components
			glm::vec3 vert1,vert2,vert3;
			vert1.x = model->theVerts[model->theFaces[tris[i]].thePoints[0]].x;
			vert1.y = model->theVerts[model->theFaces[tris[i]].thePoints[0]].y;
			vert1.z = model->theVerts[model->theFaces[tris[i]].thePoints[0]].z;
			
			vert2.x = model->theVerts[model->theFaces[tris[i]].thePoints[1]].x;
			vert2.y = model->theVerts[model->theFaces[tris[i]].thePoints[1]].y;
			vert2.z = model->theVerts[model->theFaces[tris[i]].thePoints[1]].z;
			
			vert3.x = model->theVerts[model->theFaces[tris[i]].thePoints[2]].x;
			vert3.y = model->theVerts[model->theFaces[tris[i]].thePoints[2]].y;
			vert3.z = model->theVerts[model->theFaces[tris[i]].thePoints[2]].z;
			
			glm::vec3* fuseV = getBBVertices(planeModel.at(0));
			glm::vec3* rightWingV = getBBVertices(planeModel.at(1));
			glm::vec3* rightWheelV = getBBVertices(planeModel.at(2));
			glm::vec3* leftWingV = getBBVertices(planeModel.at(3));
			glm::vec3* leftWheelV = getBBVertices(planeModel.at(4));
			glm::vec3* propV = getBBVertices(planeModel.at(5));
			glm::vec3* canV = getBBVertices(planeModel.at(6));
			
			//check these values using SATs
			float response = CollisionTests::SeperatingAxisTheorem(vert1, vert2, vert3, fuseV);
			if(response){
				collisionResponse(vert1, vert2, vert3, response);
				if(speed > 70){
					this->crashed = true;
				}
			}
			if(rightWingBroke == false){
				response = CollisionTests::SeperatingAxisTheorem(vert1, vert2, vert3, rightWingV);
				if(response){
					collisionResponse(vert1, vert2, vert3, response);
					if(speed > 60){
						rightWingBroke = true;
						rightWheelBroke = true;
					}
				}
			}
			if(!rightWheelBroke){
				response = CollisionTests::SeperatingAxisTheorem(vert1, vert2, vert3, rightWheelV);
				if(response){
					collisionResponse(vert1, vert2, vert3, response);
					wheelCollision = true;
					if(speed > 80){
						rightWheelBroke = true;
					}
				}
			}
			if(!leftWingBroke){
				response = CollisionTests::SeperatingAxisTheorem(vert1, vert2, vert3, leftWingV);
				if(response){
					collisionResponse(vert1, vert2, vert3, response);
					if(speed > 60){
						leftWingBroke = true;
						leftWheelBroke = true;
					}
				}
			}
			if(!leftWheelBroke && !wheelCollision){
				response = CollisionTests::SeperatingAxisTheorem(vert1, vert2, vert3, leftWheelV);
				if(response){
					collisionResponse(vert1, vert2, vert3, response);
					wheelCollision = true;
					if(speed > 80){
						leftWheelBroke = true;
					}
				}
			}
			response = CollisionTests::SeperatingAxisTheorem(vert1, vert2, vert3, propV);
			if(response){
				collisionResponse(vert1, vert2, vert3, response);
				if(speed > 30){
					this->crashed = true;
				}
			}
			response = CollisionTests::SeperatingAxisTheorem(vert1, vert2, vert3, canV);
			if(response){
				collisionResponse(vert1, vert2, vert3, response);
				if(speed > 50){
					this->crashed = true;
				}
			}
		}
	}
}

