//OPENGL 3.2 DEMO FOR RENDERING OBJECTS LOADED FROM OBJ FILES

#include <windows.h>		// Header File For Windows
#include "gl/glew.h"
#include "gl/wglew.h"
#pragma comment(lib, "glew32.lib")

#include "console.h"
#include "shaders/Shader.h"   // include shader header file, this is not part of OpenGL


#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtc\type_ptr.hpp"
#include "glm\gtc\matrix_inverse.hpp"
#include "glm\gtx\quaternion.hpp"
#include "glm\gtx\vector_angle.hpp"

ConsoleWindow console;
Shader* myShader;  ///shader object 
Shader* myBasicShader;
Shader* hudBasicShader;

//Cameras
#include"Camera.h"
Camera cam;

//MODEL LOADING
#include "3DStruct\threeDModel.h"
#include "Obj\OBJLoader.h"
#include "Octree\Octree.h"

//Game Objects
#include "Plane.h"
#include "Hud.h"

float amount = 0;
float temp = 0.00002f;

float pitch= 0.0f;
float roll = 0.0f;
float yaw = 0.0f;

float skySpin = 0.0f;
float propSpin = 0.0f;

Hud hud;

ThreeDModel* skyDome;
ThreeDModel* road;
vector <ThreeDModel*> buildings, planeModel, ground, fancy;
OBJLoader objLoader;
Plane plane;

///END MODEL

//OCTREES	
bool showOctrees = false;

glm::mat4 ProjectionMatrix; // matrix for the orthographic projection
glm::mat4 ModelViewMatrix;  // matrix for the modelling and viewing

//Material properties
float Material_Ambient[4] = {0.4, 0.4, 0.4, 1.0};
float Material_Diffuse[4] = {0.8, 0.8, 0.5, 1.0};
float Material_Specular[4] = {0.9,0.9,0.8,1.0};
float Material_Shininess = 50;

//Light Properties
float Light_Ambient_And_Diffuse[4] = {0.8, 0.8, 0.8, 1.0};
float Light_Specular[4] = {0.6,0.6,0.6,1.0};
float LightPos[4] = {0.7, 0.6, 0.0, 0.0};
// Fancy Light Properties
float Current_Light[4] = {0.2,0.61,0.87,1.0};

//Lighting states and change constants
float Morning_Light[4] = {0.2,0.61,0.87,1.0};
float dayR = 0.6;
float dayG = 0.19;
float dayB = -0.07;
float Daytime_Light[4] = {0.8,0.8,0.8,1.0};
float eveR = -0.55;
float eveG = -0.58;
float eveB = -0.32;
float Evening_Light[4] = {0.25,0.22,0.48,.0};
float nightR = -0.22;
float nightG = -0.12;
float nightB = -0.18;
float Night_Light[4]   = {0.03,0.1,0.3,1.0};
float mornR = 0.17;
float mornG = 0.51;
float mornB = 0.57;

//Lighting enum class
enum class TimeZone{Morning = 0, Daytime = 1, Evening = 2, Night = 3};
TimeZone lightStatus = TimeZone::Morning;

// Keys & Windows
int	mouse_x=0, mouse_y=0;
bool LeftPressed = false;
int screenWidth=1080, screenHeight=640;
bool keys[256];

//DeltaT
#include <time.h>
// Timer 
clock_t currentTime = clock();
clock_t prevTime = clock();
double deltaT = 0;
double prevDeltaT = 0;

//OPENGL FUNCTION PROTOTYPES
void display();				//called in winmain to draw everything to the screen
void reshape();				//called when the window is resized
void init();				//called in winmain when the program starts.
void processKeys();         //called in winmain to process keyboard input
void update(double deltaT);		//called in winmain to update variables
ThreeDModel* loadModel(char* modelName, Shader* myShader, float octreeDepth);
/*************    START OF OPENGL FUNCTIONS   ****************/
void init()
{


	glClearColor(1.0,1.0,1.0,0.0);						//sets the clear colour to yellow
														//glClear(GL_COLOR_BUFFER_BIT) in the display function
														//will clear the buffer to this colour
	glEnable(GL_DEPTH_TEST);
	//shader for normal models
	myShader = new Shader;
	//if(!myShader->load("BasicView", "glslfiles/basicTransformationsWithDisplacement.vert", "glslfiles/basicTransformationsWithDisplacement.frag"))
    if(!myShader->load("BasicView", "glslfiles/basicTransformations.vert", "glslfiles/basicTransformations.frag"))
	{
		cout << "failed to load shader" << endl;
	}
	//Shader for the wire frame models of the octrees
	myBasicShader = new Shader;
	if(!myBasicShader->load("Basic", "glslfiles/basic.vert", "glslfiles/basic.frag"))
	{
		cout << "failed to load shader" << endl;
	}
	//shader for the HUD
	hudBasicShader = new Shader;
	if(!hudBasicShader->load("Hud View", "glslfiles/hudBasic.vert", "glslfiles/hudBasic.frag"))
	{
		cout << "failed to load shader" << endl;
	}

	glUseProgram(myShader->handle());  // use the shader

	glEnable(GL_TEXTURE_2D);
	
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//**************LOADING Models***************************

	//BUILDINGS
	for(int i = 9; i <= 36; i++){
		std::string filename = "TestModels/Building" + std::to_string(i) + ".obj";
		char *modelName = new char[filename.length() + 1];
		strcpy(modelName, filename.c_str());
		buildings.push_back(loadModel(modelName,myShader,1.0f));
	}
	buildings.push_back(loadModel("TestModels/ControlTower.obj",myShader,2.0f));
	buildings.push_back(loadModel("TestModels/Hanger.obj",myShader,1.0f));

	//FANCY olympic rings THING
	fancy.push_back(loadModel("TestModels/Sup1.obj",myShader,3.0f));
	fancy.push_back(loadModel("TestModels/Sup2.obj",myShader,3.0f));
	fancy.push_back(loadModel("TestModels/r1.obj",myShader,3.0f));
	fancy.push_back(loadModel("TestModels/r2.obj",myShader,3.0f));
	fancy.push_back(loadModel("TestModels/r3.obj",myShader,3.0f));
	fancy.push_back(loadModel("TestModels/r4.obj",myShader,3.0f));
	fancy.push_back(loadModel("TestModels/r5.obj",myShader,3.0f));

	//PLANE
	cout << "In Load Object" << endl;
	planeModel.push_back(loadModel("TestModels/P-47.obj",myShader,3.0f));
	planeModel.push_back(loadModel("TestModels/P-47_rightWing.obj",myShader,3.0f));
	planeModel.push_back(loadModel("TestModels/P-47_rightWheels2.obj",myShader,2.0f));
	planeModel.push_back(loadModel("TestModels/P-47_leftWing.obj",myShader,3.0f));
	planeModel.push_back(loadModel("TestModels/P-47_leftWheels2.obj",myShader,2.0f));
	planeModel.push_back(loadModel("TestModels/P-47_Canopy_2.obj",myShader,3.0f));
	planeModel.push_back(loadModel("TestModels/P-47_Prop.obj",myShader,3.0f));

	//GROUND
	ground.push_back(loadModel("TestModels/Floor.obj", myShader,1.0f));
	ground.push_back(loadModel("TestModels/Ground.obj", myShader,1.0f));
	road = loadModel("TestModels/Roads.obj",myShader,3.0f);

	////SKY DOME
	skyDome = loadModel("TestModels/SkyDome2.obj",myShader,1);

	//create plane, camera and hud objects
	plane = Plane(myBasicShader, planeModel);
	cam = Camera(&plane, deltaT);

	hud = Hud(hudBasicShader, &plane);

}

void display()									
{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
		glMatrixMode(GL_MODELVIEW);

		glUseProgram(myShader->handle());  // use the shader

		amount += temp;
		if(amount > 1 || amount < -0.5)
			temp = -temp;
		//amount = 0;
		glUniform1f(glGetUniformLocation(myShader->handle(), "displacement"), amount);
	
		GLuint matLocation = glGetUniformLocation(myShader->handle(), "ProjectionMatrix");  
		glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);
	
		//CAMERA
		glm::mat4 viewingMatrix = glm::lookAt(cam.getPos(), cam.getLookat(), cam.getUp());
	
		glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ViewMatrix"), 1, GL_FALSE, &viewingMatrix[0][0]);
	
		glm::mat4 lightMat = glm::rotate(glm::mat4(1.0),skySpin,glm::vec3(0,1,0));
		glm::vec4 lightRotated = lightMat * glm::vec4(LightPos[0],LightPos[1],LightPos[2],LightPos[3]);
		float lightSpinPos [4] = {lightRotated.x,lightRotated.y,lightRotated.z,lightRotated.w};
		glUniform4fv(glGetUniformLocation(myShader->handle(), "LightPos"), 1, lightSpinPos);
		glUniform4fv(glGetUniformLocation(myShader->handle(), "light_ambient"), 1, Current_Light);
		glUniform4fv(glGetUniformLocation(myShader->handle(), "light_diffuse"), 1, Current_Light);
		glUniform4fv(glGetUniformLocation(myShader->handle(), "light_specular"), 1, Light_Specular);
	
		glUniform4fv(glGetUniformLocation(myShader->handle(), "material_ambient"), 1, Material_Ambient);
		glUniform4fv(glGetUniformLocation(myShader->handle(), "material_diffuse"), 1, Material_Diffuse);
		glUniform4fv(glGetUniformLocation(myShader->handle(), "material_specular"), 1, Material_Specular);
		Material_Shininess = 0.9;
		glUniform1f(glGetUniformLocation(myShader->handle(), "material_shininess"), Material_Shininess);

		//***********GROUND*******************************
		glUseProgram(myShader->handle());

		matLocation = glGetUniformLocation(myShader->handle(), "ProjectionMatrix");  
		glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);
	
		ModelViewMatrix = glm::translate(viewingMatrix, glm::vec3(0,0,0));
	
		glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
	
	
		glm::mat3 normalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
		glUniformMatrix3fv(glGetUniformLocation(myShader->handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);
	
		for each (ThreeDModel* m in ground){
			m->drawElementsUsingVBO(myShader);
		}
		for each(ThreeDModel* m in buildings){
			m->drawElementsUsingVBO(myShader);
		}
		for each(ThreeDModel* m in fancy){
			m->drawElementsUsingVBO(myShader);
		}
		road ->drawElementsUsingVBO(myShader);
	
		ModelViewMatrix = glm::rotate(ModelViewMatrix, skySpin, glm::vec3(0,1,0));
		ModelViewMatrix =glm::translate(ModelViewMatrix,glm::vec3(0,-300,0));
		glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
		normalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
		glUniformMatrix3fv(glGetUniformLocation(myShader->handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);
	
		skyDome->drawElementsUsingVBO(myShader);
		
		//*************OCTREE DRAWING*********************************
		if(showOctrees){
			glUseProgram(myBasicShader->handle());  // use the shader
			glUniformMatrix4fv(glGetUniformLocation(myBasicShader->handle(), "ProjectionMatrix"), 1, GL_FALSE, &ProjectionMatrix[0][0]);
			glUniformMatrix4fv(glGetUniformLocation(myBasicShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
		
			for each(ThreeDModel* m in buildings){
				m->drawBoundingBox(myBasicShader);
			}
			for each(ThreeDModel* m in fancy){
				m->drawBoundingBox(myBasicShader);
			}
			for each(ThreeDModel* m in ground){
				m->drawBoundingBox(myBasicShader);
			}
		
			glUseProgram(myShader->handle());  // use the shader
		}

		//********************END GROUND************************
	
		//**********************PLANE***************************
		Material_Shininess = 50;
		glUniform1f(glGetUniformLocation(myShader->handle(), "material_shininess"), Material_Shininess);
		//**********************************PROPELLER************************
		ModelViewMatrix = glm::translate(viewingMatrix, plane.getPosition());
	
		glm::vec3 currentRot = plane.getRot();
		glm::quat currentQuat = plane.getCurrentQuat();

		//update currentRot in processKeys where W is currentRot.x += angle. S is currentRot -= angle;

		glm::quat tempQuat = glm::quat(currentRot);
		currentQuat = currentQuat * tempQuat;
		plane.setCurrentQuat(currentQuat);
	
		glm::mat4 rotate = glm::toMat4(currentQuat);

		ModelViewMatrix = ModelViewMatrix * rotate;

		ModelViewMatrix = glm::rotate(ModelViewMatrix, propSpin, glm::vec3(0,0,1));
		
		glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
		normalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
		glUniformMatrix3fv(glGetUniformLocation(myShader->handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);

		planeModel.at(6)->drawElementsUsingVBO(myShader);
	
		//*******************************REST OF PLANE ******************
	
		ModelViewMatrix = glm::translate(viewingMatrix, plane.getPosition());
	
		currentRot = plane.getRot();
		currentQuat = plane.getCurrentQuat();

		//update currentRot in processKeys where W is currentRot.x += angle. S is currentRot -= angle;

		tempQuat = glm::quat(currentRot);
		currentQuat = currentQuat * tempQuat;
		plane.setCurrentQuat(currentQuat);
	
		rotate = glm::toMat4(currentQuat);
	
		ModelViewMatrix = ModelViewMatrix * rotate;
	
		glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
	    normalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
		glUniformMatrix3fv(glGetUniformLocation(myShader->handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);
	
		planeModel.front()->drawElementsUsingVBO(myShader);
		if(!plane.getRightWingStatus())
			planeModel.at(1)->drawElementsUsingVBO(myShader);
		if(!plane.getRightWheelStatus())
			planeModel.at(2)->drawElementsUsingVBO(myShader);
		if(!plane.getLeftWingStatus())
			planeModel.at(3)->drawElementsUsingVBO(myShader);
		if(!plane.getLeftWheeltatus())
			planeModel.at(4)->drawElementsUsingVBO(myShader);
		planeModel.at(5)->drawElementsUsingVBO(myShader);
		//planeModel.at(6)->drawElementsUsingVBO(myShader);
	
		//*************OCTREE DRAWING*********************************
		if(showOctrees){
			glUseProgram(myBasicShader->handle());  // use the shader
			glUniformMatrix4fv(glGetUniformLocation(myBasicShader->handle(), "ProjectionMatrix"), 1, GL_FALSE, &ProjectionMatrix[0][0]);
			glUniformMatrix4fv(glGetUniformLocation(myBasicShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
	
			for each (ThreeDModel* m in planeModel){
				m->drawBoundingBox(myBasicShader);
				//m->drawOctreeLeaves(myBasicShader);
			}
	
			plane.draw();
			glUseProgram(myShader->handle());  // use the shader
		}

		plane.resetRot();

		//***********END PLANE*******************************
		//**************HUD**********************************
		glUseProgram(hudBasicShader->handle());  // use the shader

		hud.draw();
	
		glUseProgram(0);
		glFlush();

}

void update(double deltaT)
{
	//only update plane if it hasnt crashed
	if(!plane.getCrashed()){
		plane.update(deltaT);
		plane.detectCollisionRoot(buildings);
		plane.detectCollisionRoot(fancy);
		plane.detectCollisionRoot(ground);
		propSpin += (20*plane.getSpeed()) * deltaT;
		if(propSpin > 360)
			propSpin = 0;
	}
	//update camera and HUD
	cam.update(&plane, deltaT);
	
	hud.update(deltaT,plane.getSpeed(),plane.getPosition().y, plane.getRot().z, plane.getCrashed());

	//spin the sky dome
	skySpin += 1.0f* deltaT;
	if(skySpin > 360)
		skySpin = 0;

	//Update Lighting
	switch(lightStatus){
	case(TimeZone::Morning):{
		cout << "Morning" << endl;
		if(Current_Light[0] != Daytime_Light[0])
			Current_Light[0] += (dayR * deltaT/60);
		if(Current_Light[1] != Daytime_Light[1])
			Current_Light[1] += (dayG * deltaT/60);
		if(Current_Light[2] != Daytime_Light[2])
			Current_Light[2] += (dayB * deltaT/60);
		cout << Current_Light[0] << " , " << Current_Light[1] << " , " << Current_Light[2] << endl;
		if(Current_Light[0] >= Daytime_Light[0] && Current_Light[1] >= Daytime_Light[1] && Current_Light[2] >= Daytime_Light[2])
			lightStatus = TimeZone::Daytime;
		break;
		}
	case(TimeZone::Daytime):{
		cout << "Daytime" << endl;
		if(Current_Light[0] != Evening_Light[0])
			Current_Light[0] += (eveR * deltaT/60);
		if(Current_Light[1] != Evening_Light[1])
			Current_Light[1] += (eveG * deltaT/60);
		if(Current_Light[2] != Evening_Light[2])
			Current_Light[2] += (eveB * deltaT/60);
		if(Current_Light[0] <= Evening_Light[0] && Current_Light[1] <= Evening_Light[1] && Current_Light[2] <= Evening_Light[2])
			lightStatus = TimeZone::Evening;
		break;
		}
	case(TimeZone::Evening):{
		cout << "Evening" << endl;
		if(Current_Light[0] != Night_Light[0])
			Current_Light[0] += (nightR * deltaT/60);
		if(Current_Light[1] != Night_Light[1])
			Current_Light[1] += (nightG * deltaT/60);
		if(Current_Light[2] != Night_Light[2])
			Current_Light[2] += (nightB * deltaT/60);
		if(Current_Light[0] <= Night_Light[0] && Current_Light[1] <= Night_Light[1] && Current_Light[2] <= Night_Light[2])
			lightStatus = TimeZone::Night;
		break;
		}
	case(TimeZone::Night):{
		cout << "Night" << endl;
		if(Current_Light[0] != Morning_Light[0])
			Current_Light[0] += (mornR * deltaT/60);
		if(Current_Light[1] != Morning_Light[1])
			Current_Light[1] += (mornG * deltaT/60);
		if(Current_Light[2] != Morning_Light[2])
			Current_Light[2] += (mornB * deltaT/60);
		if(Current_Light[0] >= Morning_Light[0] && Current_Light[1] >= Morning_Light[1] && Current_Light[2] >= Morning_Light[2])
			lightStatus = TimeZone::Morning;
		break;
		}
	}
	}

void reshape(int width, int height)		// Resize the OpenGL window
{
	screenWidth=width; screenHeight = height;           // to ensure the mouse coordinates match 
														// we will use these values to set the coordinate system

	glViewport(0,0,width,height);						// Reset The Current Viewport

	//Set the projection matrix
	ProjectionMatrix = glm::perspective(60.0f, (GLfloat)screenWidth/(GLfloat)screenHeight, 0.1f, 10000.0f);
}

ThreeDModel* loadModel(char* modelName, Shader* myShader, float octreeDepth){

	ThreeDModel *model = new ThreeDModel();

	cout << " loading Object " << endl;
	if(objLoader.loadModel(modelName, *model))//returns true if the model is loaded, puts the model in the model parameter
	{
		cout << " Object loaded " << endl;
		model->calcVertNormalsUsingOctree();  //the method will construct the octree if it hasn't already been created.
		//turn on VBO by setting useVBO to true in threeDmodel.cpp default constructor - only permitted on 8 series cards and higher
		model->initDrawElements();
		model->initVBO(myShader);
		//model->deleteVertexFaceData(); NO BAD CODE
		return model;
	}
	else
	{
		cout << " Object failed to load " << endl;
	}

}

void processKeys()
{
	//Show Octrees
	if(keys['O'])
	{
		showOctrees = !showOctrees;
		keys['O'] = false;
	}
	//Camera Selection
	if(keys['1'])
	{
		cam.switchCam(Status::First);
	}
	if(keys['2'])
	{
		cam.switchCam(Status::Third);
	}
	if(keys['3'])
	{
		cam.switchCam(Status::Static);
	}
	glm::vec3 rot = plane.getRot();
	if(!plane.getCrashed()){
		const double angle = 0.5;
		const double speed = 0.1;
		float rotate = angle*deltaT;
		//Yaw
		if (keys['Q'])
		{
			rot.y = plane.yaw(1,rot.y);
		}
		if (keys['E'])
		{
			rot.y = plane.yaw(0,rot.y);
		}
		//Pitch
		if (keys['W'])
		{
			rot.x = plane.pitch(1,rot.x);
		}
		if (keys['S'])
		{
			rot.x = plane.pitch(0,rot.x);
		}
		//Roll
		if (keys['A'])
		{
			rot.z = plane.pitch(0,rot.z);
		}
		if (keys['D'])
		{
			rot.z = plane.pitch(1,rot.z);
		}
	
		//Speed
		if (keys[VK_UP])
		{
			if(!plane.getStalledStatus()){
				plane.setSpeed(plane.getSpeed() + speed);
				if(plane.getSpeed() > 120)
					plane.setSpeed(120);
			}
			//keys[VK_UP] = false;
		}
		if (keys[VK_DOWN])
		{
			if(!plane.getStalledStatus()){
				plane.setSpeed(plane.getSpeed() - speed);
				if(plane.getSpeed() < -10)
					plane.setSpeed(-10);
			}
			//keys[VK_DOWN] = false;
		}
		rot = plane.specialCases(rot);
	}
	
	//Respawn
	if(plane.getCrashed()){
		if(keys[VK_SPACE]){
			plane = Plane(myBasicShader, planeModel);
			hud.resetHud();
			Current_Light[0] = Morning_Light[0];
			Current_Light[1] = Morning_Light[1];
			Current_Light[2] = Morning_Light[2];
			lightStatus = TimeZone::Morning;
		}
	}
	
	//Camera Panning
	if (keys[VK_LEFT])
	{
		cam.panLeft();
		//keys[VK_UP] = false;
	}
	if (keys[VK_RIGHT])
	{
		cam.panRight();
	}
	if(!keys[VK_RIGHT] && ! keys[VK_LEFT])
	{
		cam.unPan();
	}

	
	
	//cout << "Stalled Status = " << plane.getStalledStatus() << endl;
	//cout << "Landed Status = " << plane.getLandedStatus() << endl;
	
	plane.setRot(rot);
}
/**************** END OPENGL FUNCTIONS *************************/







//WIN32 functions
LRESULT	CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);	// Declaration For WndProc
void KillGLWindow();									// releases and destroys the window
bool CreateGLWindow(char* title, int width, int height); //creates the window
int WINAPI WinMain(	HINSTANCE, HINSTANCE, LPSTR, int);  // Win32 main function

//win32 global variabless
HDC			hDC=NULL;		// Private GDI Device Context
HGLRC		hRC=NULL;		// Permanent Rendering Context
HWND		hWnd=NULL;		// Holds Our Window Handle
HINSTANCE	hInstance;		// Holds The Instance Of The Application


/******************* WIN32 FUNCTIONS ***************************/
int WINAPI WinMain(	HINSTANCE	hInstance,			// Instance
					HINSTANCE	hPrevInstance,		// Previous Instance
					LPSTR		lpCmdLine,			// Command Line Parameters
					int			nCmdShow)			// Window Show State
{
	MSG		msg;									// Windows Message Structure
	bool	done=false;								// Bool Variable To Exit Loop

	console.Open();


	// Create Our OpenGL Window
	if (!CreateGLWindow("P-47 Flight Simulator",screenWidth,screenHeight))
	{
		return 0;									// Quit If Window Was Not Created
	}

	while(!done)									// Loop That Runs While done=FALSE
	{
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	// Is There A Message Waiting?
		{
			if (msg.message==WM_QUIT)				// Have We Received A Quit Message?
			{
				done=true;							// If So done=TRUE
			}
			else									// If Not, Deal With Window Messages
			{
				TranslateMessage(&msg);				// Translate The Message
				DispatchMessage(&msg);				// Dispatch The Message
			}
		}
		else										// If There Are No Messages
		{
			if(keys[VK_ESCAPE])
				done = true;

			processKeys();			//process keyboard

			//Timer
			//get current time
			currentTime = clock();
			clock_t clockTicksTaken = currentTime - prevTime;
			deltaT = (clockTicksTaken / (double)CLOCKS_PER_SEC);					

			//// Advance timer
			prevTime = currentTime;					// use the current time as the previous time in the next step
			prevDeltaT = deltaT;					// use the current deltaT as the previous deltaT in the next step

			update(deltaT);					// update variables
			display();					// Draw The Scene
			
			SwapBuffers(hDC);				// Swap Buffers (Double Buffering)
		}
	}

	console.Close();

	// Shutdown
	KillGLWindow();									// Kill The Window
	return (int)(msg.wParam);						// Exit The Program
}

//WIN32 Processes function - useful for responding to user inputs or other events.
LRESULT CALLBACK WndProc(	HWND	hWnd,			// Handle For This Window
							UINT	uMsg,			// Message For This Window
							WPARAM	wParam,			// Additional Message Information
							LPARAM	lParam)			// Additional Message Information
{
	switch (uMsg)									// Check For Windows Messages
	{
		case WM_CLOSE:								// Did We Receive A Close Message?
		{
			PostQuitMessage(0);						// Send A Quit Message
			return 0;								// Jump Back
		}
		break;

		case WM_SIZE:								// Resize The OpenGL Window
		{
			reshape(LOWORD(lParam),HIWORD(lParam));  // LoWord=Width, HiWord=Height
			return 0;								// Jump Back
		}
		break;

		case WM_LBUTTONDOWN:
			{
	            mouse_x = LOWORD(lParam);          
				mouse_y = screenHeight - HIWORD(lParam);
				LeftPressed = true;
			}
		break;

		case WM_LBUTTONUP:
			{
	            LeftPressed = false;
			}
		break;

		case WM_MOUSEMOVE:
			{
	            mouse_x = LOWORD(lParam);          
				mouse_y = screenHeight  - HIWORD(lParam);
			}
		break;
		case WM_KEYDOWN:							// Is A Key Being Held Down?
		{
			keys[wParam] = true;					// If So, Mark It As TRUE
			return 0;								// Jump Back
		}
		break;
		case WM_KEYUP:								// Has A Key Been Released?
		{
			keys[wParam] = false;					// If So, Mark It As FALSE
			return 0;								// Jump Back
		}
		break;
	}

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

void KillGLWindow()								// Properly Kill The Window
{
	if (hRC)											// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL,NULL))					// Are We Able To Release The DC And RC Contexts?
		{
			MessageBox(NULL,"Release Of DC And RC Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(hRC))						// Are We Able To Delete The RC?
		{
			MessageBox(NULL,"Release Rendering Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}
		hRC=NULL;										// Set RC To NULL
	}

	if (hDC && !ReleaseDC(hWnd,hDC))					// Are We Able To Release The DC
	{
		MessageBox(NULL,"Release Device Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hDC=NULL;										// Set DC To NULL
	}

	if (hWnd && !DestroyWindow(hWnd))					// Are We Able To Destroy The Window?
	{
		MessageBox(NULL,"Could Not Release hWnd.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hWnd=NULL;										// Set hWnd To NULL
	}

	if (!UnregisterClass("OpenGL",hInstance))			// Are We Able To Unregister Class
	{
		MessageBox(NULL,"Could Not Unregister Class.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hInstance=NULL;									// Set hInstance To NULL
	}
}

/*	This Code Creates Our OpenGL Window.  Parameters Are:					*
 *	title			- Title To Appear At The Top Of The Window				*
 *	width			- Width Of The GL Window Or Fullscreen Mode				*
 *	height			- Height Of The GL Window Or Fullscreen Mode			*/
 
bool CreateGLWindow(char* title, int width, int height)
{
	GLuint		PixelFormat;			// Holds The Results After Searching For A Match
	WNDCLASS	wc;						// Windows Class Structure
	DWORD		dwExStyle;				// Window Extended Style
	DWORD		dwStyle;				// Window Style
	RECT		WindowRect;				// Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left=(long)0;			// Set Left Value To 0
	WindowRect.right=(long)width;		// Set Right Value To Requested Width
	WindowRect.top=(long)0;				// Set Top Value To 0
	WindowRect.bottom=(long)height;		// Set Bottom Value To Requested Height

	hInstance			= GetModuleHandle(NULL);				// Grab An Instance For Our Window
	wc.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraw On Size, And Own DC For Window.
	wc.lpfnWndProc		= (WNDPROC) WndProc;					// WndProc Handles Messages
	wc.cbClsExtra		= 0;									// No Extra Window Data
	wc.cbWndExtra		= 0;									// No Extra Window Data
	wc.hInstance		= hInstance;							// Set The Instance
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);			// Load The Default Icon
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	wc.hbrBackground	= NULL;									// No Background Required For GL
	wc.lpszMenuName		= NULL;									// We Don't Want A Menu
	wc.lpszClassName	= "OpenGL";								// Set The Class Name

	if (!RegisterClass(&wc))									// Attempt To Register The Window Class
	{
		MessageBox(NULL,"Failed To Register The Window Class.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;											// Return FALSE
	}
	
	dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// Window Extended Style
	dwStyle=WS_OVERLAPPEDWINDOW;							// Windows Style
	
	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);		// Adjust Window To True Requested Size

	// Create The Window
	if (!(hWnd=CreateWindowEx(	dwExStyle,							// Extended Style For The Window
								"OpenGL",							// Class Name
								title,								// Window Title
								dwStyle |							// Defined Window Style
								WS_CLIPSIBLINGS |					// Required Window Style
								WS_CLIPCHILDREN,					// Required Window Style
								0, 0,								// Window Position
								WindowRect.right-WindowRect.left,	// Calculate Window Width
								WindowRect.bottom-WindowRect.top,	// Calculate Window Height
								NULL,								// No Parent Window
								NULL,								// No Menu
								hInstance,							// Instance
								NULL)))								// Dont Pass Anything To WM_CREATE
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Window Creation Error.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	static	PIXELFORMATDESCRIPTOR pfd=				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		24,										// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		24,											// 24Bit Z-Buffer (Depth Buffer)  
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};
	
	if (!(hDC=GetDC(hWnd)))							// Did We Get A Device Context?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Device Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!(PixelFormat=ChoosePixelFormat(hDC,&pfd)))	// Did Windows Find A Matching Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Find A Suitable PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if(!SetPixelFormat(hDC,PixelFormat,&pfd))		// Are We Able To Set The Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Set The PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	HGLRC tempContext = wglCreateContext(hDC);
	wglMakeCurrent(hDC, tempContext);

	glewInit();

	int attribs[] =
	{
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
		WGL_CONTEXT_MINOR_VERSION_ARB, 1,
		WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
		0
	};
	
    if(wglewIsSupported("WGL_ARB_create_context") == 1)
    {
		hRC = wglCreateContextAttribsARB(hDC,0, attribs);
		wglMakeCurrent(NULL,NULL);
		wglDeleteContext(tempContext);
		wglMakeCurrent(hDC, hRC);
	}
	else
	{	//It's not possible to make a GL 3.x context. Use the old style context (GL 2.1 and before)
		hRC = tempContext;
		cout << " not possible to make context "<< endl;
	}

	//Checking GL version
	const GLubyte *GLVersionString = glGetString(GL_VERSION);

	cout << GLVersionString << endl;

	//OpenGL 3.2 way of checking the version
	int OpenGLVersion[2];
	glGetIntegerv(GL_MAJOR_VERSION, &OpenGLVersion[0]);
	glGetIntegerv(GL_MINOR_VERSION, &OpenGLVersion[1]);

	cout << OpenGLVersion[0] << " " << OpenGLVersion[1] << endl;

	ShowWindow(hWnd,SW_SHOW);						// Show The Window
	SetForegroundWindow(hWnd);						// Slightly Higher Priority
	SetFocus(hWnd);									// Sets Keyboard Focus To The Window
	reshape(width, height);					// Set Up Our Perspective GL Screen

	init();
	
	return true;									// Success
}